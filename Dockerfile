FROM debian:stretch

RUN apt update && \
    apt install -y build-essential

RUN apt install -y wget sudo gnupg2

RUN echo "deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-6.0 main" > /etc/apt/sources.list.d/clang.list
RUN echo "deb-src http://apt.llvm.org/stretch/ llvm-toolchain-stretch-6.0 main" >> /etc/apt/sources.list.d/clang.list

RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key|sudo apt-key add - && \
    apt update && \
    apt-get install -y clang-6.0 lldb-6.0 lld-6.0




ADD main.cpp .

RUN clang++-6.0 --target=wasm32-unknown-unknown-wasm -std=c++11 main.cpp